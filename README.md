PostgreSQL install Ansible Role
=========

Playbook for postgreSQL

Requirements
----------------
netaddr ansible package

Role Variables
----------------

#### validate certs for postgresql repo key
postgresql_validate_certs: "yes"

#### postgresql apt key name
postgresql_apt_key: "ACCC4CF8.asc"

#### version of postgresql (9.1, 9.2, 9.3, 9.4, 9.5, 9.6, 10, 11, 12, etc)
postgresql_version: 15

#### Database port to connect to
postgresql_port: 5432

#### Path to postgresql.conf
postgresql_config_path: "/etc/postgresql/{{ postgresql_version }}/main"
#### Options for postgresql config
```
postgresql_config_options:
  - option: listen_addresses
    value: "*"
    state: 'present' # present (default)| absent
  - option: port
    value: "{{ postgresql_port }}"
```
#### Options for createcluster config, used for creating folders' tree
postgresql_cluster_options: []

#### list of database users
```
postgresql_users: []
#  - name:
#    password:
#    encrypted: no (optional)
#    role_attr_flags: NOSUPERUSER (optional) http://docs.ansible.com/ansible/latest/postgresql_user_module.html#options
```

#### list of databases
```
postgresql_dbs: []
#  - db:
#    user:
#    encoding: UNICODE (optional)
#    ext: (optional)
#      - ext1
#      - ext2
#    schema: (optional)
#      - schema1
#      - schema2
  ```

#### flag to create the databases
postgresql_create_db: true

#### path to template of pg_hba.conf
postgresql_pghba_template_path: "{{ role_path }}/templates/pg_hba.conf.tpl"

Example Playbook
----------------

main.yml
```
---
- hosts: db
  roles:
    - sample.ansible_role_postgresql
```

group_vars/db/main.yml
```
---
postgresql_users:
  - user: test
    password: "strong_password_1"

postgresql_dbs:
   - db: test_db
     user: test
     host: 1.0.0.29/32
```
License
----------------
GPLv3

Author Information
------------------
* Kirill Fedorov as a challege for ClassBook project https://deusops.com/classbook
* This role is based on public role from Konstantin Deepezh, https://t.me/deusops